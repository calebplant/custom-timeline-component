import { api, LightningElement } from 'lwc';

const BASE_CLASS = 'slds-timeline__item_expandable slds-timeline__item_';

export default class TimelineItem extends LightningElement {
    @api details;

    isCollapsed = true;

    get timelineClass() {
        return this.isCollapsed ? BASE_CLASS + this.timelineItemClassType : BASE_CLASS + this.timelineItemClassType + ' slds-is-open';
    }

    get timelineItemClassType() {
        switch(this.details.subtype) {
            case 'Task':
                return 'task';
            case 'Email':
            case 'List Email':
                return 'email';
            case 'Call':
                return 'call';
            case 'Event':
                return 'event';
        }
    }

    get iconName() {
        switch(this.details.subtype) {
            case 'Task':
                return 'standard:task';
            case 'Email':
            case 'List Email':
                return 'standard:email';
            case 'Call':
                return 'standard:log_a_call';
            case 'Event':
                return 'standard:event';

        }
    }

    get showRelatedTo() {
        return this.details.relatedTo.length > 0 ? true : false;
    }

    // Handlers
    handleToggle() {
        console.log('timelineItem :: handleToggle');
        this.isCollapsed = this.isCollapsed ? false : true;
    }
}