import { LightningElement, wire } from 'lwc';
import getActivities from '@salesforce/apex/TimelineController.getActivities';
// import getActivitiesInRange from '@salesforce/apex/TimelineController.getActivitiesInRange';

export default class TimelineComponent extends LightningElement {

    activityData;
    // startDate;
    // endDate;

    // connectedCallback() {
    //     let start = new Date();
    //     start.setDate(start.getDate() - 7);
    //     this.startDate = start.toLocaleDateString();

    //     let end = new Date();
    //     end.setDate(end.getDate() + 7);
    //     this.endDate = end.toLocaleDateString();
    // }

    @wire(getActivities)
    wiredActivities({error, data}) {
        if(data) {
            this.activityData = [];
            console.log('Response from server');
            console.log(data);
            this.activityData = data;
        } else if (error) {
            console.log('Error getting activities!');
            console.log(error);
        }
    }

    
    // @wire (getActivitiesInRange, {startDateStr: '$startDate', endDateStr: '$endDate'})
    // getActivitiesInRange(response) {
    //     if(response.data) {
    //         console.log('Successfully got activities:');
    //         console.log(response.data);
    //         if(response.data.length > 0) {
    //             this.activityData = response.data;
    //         }
    //     }
    //     if(response.error) {
    //         console.log('Error getting activities:');
    //         console.log(response.error);
    //     }
    // }
    
    // // Handlers
    // handleStartDateChange(event) {
    //     let start = new Date(Date.parse(event.target.value));
    //     console.log('start:');
    //     console.log(start.toLocaleDateString());
    //     this.startDate = start.toLocaleDateString();
    //     console.log('end?');
    // }

    // handleEndtDateChange(event) {
    //     let end = new Date(Date.parse(event.target.value));
    //     console.log('end:');
    //     console.log(end.toLocaleDateString());
    //     this.endDate = end.toLocaleDateString();
    //     console.log('end?');
    // }
    
}