import { api, LightningElement } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class RecordLink extends NavigationMixin(LightningElement) {
    @api recordId;
    @api objectApiName;
    @api displayName;

    _url;

    connectedCallback() {
        // Generate a URL to record page
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.recordId,
                objectApiName: this.objectApiName,
                actionName: 'view',
            },
        }).then(url => {
            this._url = url;
        });
    }

    navigateToRecord() {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.recordId,
                objectApiName: this.objectApiName,
                actionName: 'view'
            }
        });
    }

    // Handlers
    handleClick(event) {
        event.preventDefault();
        event.stopPropagation();
        this.navigateToRecord();
    }
}