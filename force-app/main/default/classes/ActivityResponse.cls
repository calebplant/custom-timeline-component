public with sharing class ActivityResponse implements Comparable {

    @AuraEnabled
    public Id id;
    @AuraEnabled
    public String objectApiName;
    @AuraEnabled
    public String subject;
    @AuraEnabled
    public Datetime actDate;
    @AuraEnabled
    public String displayDate;
    @AuraEnabled
    public String priority;
    @AuraEnabled
    public String description;
    @AuraEnabled
    public List<RelatedData> relatedTo = new List<RelatedData>();
    @AuraEnabled
    public String subtype;

    public ActivityResponse(Task tsk) {
        objectApiName = 'Task';
        actDate = tsk.CompletedDateTime != null ? tsk.CompletedDateTime : DateTime.newInstance(tsk.ActivityDate, Time.newInstance(0,0,0,0));
        displayDate = actDate.date().format();
        subtype = tsk.TaskSubtype;
        setCommonAttributes(tsk);
        priority = tsk.Priority;
    }

    public ActivityResponse(Event ev) {
        objectApiName = 'Event';
        actDate = ev.StartDateTime;
        displayDate = actDate.date().format();
        subtype = 'Event';
        setCommonAttributes(ev);
    }

    private void setCommonAttributes(SObject obj) {
        System.debug('get common attributes');

        id = (Id) obj.get('Id');
        subject = (String) obj.get('Subject');
        description = (String) obj.get('Description');

        if(obj.get('WhoId') != null) {
            Id objId = (Id)obj.get('WhoId');
            String recordName = (String)obj.getSobject('Who').get('Name');
            String sObjName = objId.getSObjectType().getDescribe().getName();
            relatedTo.add(new RelatedData(objId, sObjName, recordName));
        }
        if(obj.get('WhatId') != null) {
            Id objId = (Id)obj.get('WhatId');
            String recordName = (String)obj.getSobject('What').get('Name');
            String sObjName = objId.getSObjectType().getDescribe().getName();
            relatedTo.add(new RelatedData(objId, sObjName, recordName));
        }
    }

    public Integer CompareTo(Object ObjToCompare) {
        ActivityResponse that = (ActivityResponse)ObjToCompare;
        if (this.actDate > that.actDate) return 1;
        if (this.actDate < that.actDate) return -1;
        return 0;
    }
}
