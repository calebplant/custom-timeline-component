@isTest
public with sharing class TimelineController_Test {

    @TestSetup
    static void makeData(){
        Account acc = new Account(Name='TestAcc1');
        insert acc;
        Contact con = new Contact(LastName='TestCon1', AccountId=acc.Id);
        insert con;
    }
    
    @isTest
    static void doesGetActivitiesReturnActivitiesAndRelatedEntities()
    {
        // Insert task with related account and contact
        Task tsk = new Task();
        tsk.OwnerId = Userinfo.getUserId();
        tsk.Priority = 'High';
        tsk.Status = 'New';
        tsk.Subject = 'TestTask1';
        tsk.WhatId = [SELECT Id FROM Account WHERE Name = 'TestAcc1' LIMIT 1].Id;
        tsk.whoId = [SELECT Id FROM Contact WHERE Name = 'TestCon1' LIMIT 1].Id;
        tsk.ActivityDate = Date.today().addYears(-100);
        insert tsk;

        Test.startTest();
        List<ActivityResponse> res = TimelineController.getActivities();
        Test.stopTest();

        System.assertEquals(2, res[0].relatedTo.size());
        System.assertEquals('TestTask1', res[0].subject);
    }
}
