public with sharing class TimelineController {
    
    @AuraEnabled(cacheable=true)
    public static List<ActivityResponse> getActivities(){
        List<ActivityResponse> response = new List<ActivityResponse>();
        // Tasks
        List<Task> tasks = [SELECT Id, Subject, Description, Priority, TaskSubtype, WhoId, Who.Name,
                                    WhatId, What.Name, ActivityDate, CompletedDateTime
                            FROM Task
                            WHERE OwnerId = :UserInfo.getUserId()];
        // Events
        List<Event> events = [SELECT Id, Subject, Description, WhoId, Who.Name,
                                    WhatId, What.Name, StartDateTime
                              FROM Event
                              WHERE OwnerId = :UserInfo.getUserId()];

        for(Task eachTask : tasks) {
            if(eachTask.ActivityDate != null || eachTask.CompletedDateTime != null) {
                response.add(new ActivityResponse(eachTask));
            }
        }
        for(Event eachEvent : events) {
            if(eachEvent.StartDateTime != null) {
                response.add(new ActivityResponse(eachEvent));
            }
        }

        for(ActivityResponse eachRes : response) {
            System.debug(eachRes);
        }

        response.sort();
        return response;
    }

    // @AuraEnabled(cacheable=true)
    // public static List<ActivityResponse> getActivitiesInRange(String startDateStr, String endDateStr){
    //     List<ActivityResponse> response = new List<ActivityResponse>();
    //     if(startDateStr == null || endDateStr == null) { 
    //         return response;
    //     }

    //     System.debug('startDateStr: ' + startDateStr);
    //     System.debug('endDateStr: ' + endDateStr);


    //     Date startDate = Date.parse(startDateStr);
    //     Datetime startDateTime = DateTime.newInstance(startDate, Time.newInstance(0,0,0,0));
    //     Date endDate = Date.parse(endDateStr);
    //     Datetime endDateTime = DateTime.newInstance(endDate, Time.newInstance(0,0,0,0));


    //     // Tasks
    //     List<Task> tasks = [SELECT Id, Subject, Description, Priority, TaskSubtype, WhoId, Who.Name,
    //                                 WhatId, What.Name, ActivityDate, CompletedDateTime
    //                         FROM Task
    //                         WHERE OwnerId = :UserInfo.getUserId()
    //                         AND (
    //                             (ActivityDate >= :startDate AND ActivityDate <= :endDate)
    //                             OR (CompletedDateTime >= :startDateTime AND CompletedDateTime <= :endDateTime)
    //                             )];
    //     // Events
    //     List<Event> events = [SELECT Id, Subject, Description, WhoId, Who.Name,
    //                                 WhatId, What.Name, StartDateTime
    //                           FROM Event
    //                           WHERE OwnerId = :UserInfo.getUserId()
    //                           AND (StartDateTime >= :startDateTime AND StartDateTime <= :endDateTime)];

    //     for(Task eachTask : tasks) {
    //         if(eachTask.ActivityDate != null || eachTask.CompletedDateTime != null) {
    //             response.add(new ActivityResponse(eachTask));
    //         }
    //     }
    //     for(Event eachEvent : events) {
    //         if(eachEvent.StartDateTime != null) {
    //             response.add(new ActivityResponse(eachEvent));
    //         }
    //     }

    //     for(ActivityResponse eachRes : response) {
    //         System.debug(eachRes);
    //     }

    //     response.sort();
    //     return response;
    // }
}
