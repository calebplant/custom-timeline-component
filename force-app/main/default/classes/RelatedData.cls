public with sharing class RelatedData {
    @AuraEnabled
    public Id id;
    @AuraEnabled
    public String objApiName;
    @AuraEnabled
    public String name;

    public RelatedData(Id id, String objApiName, String name)
    {
        this.id = id;
        this.objApiName = objApiName;
        this.name = name;
    }
}
