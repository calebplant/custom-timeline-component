# Custom Timeline Component

## Overview

A simple LWC for displaying a custom timeline of the user's activities (tasks, events, calls, emails) as per instructions.

## Demo (~40 sec)

[Here's a link to a demo video for the component](https://www.youtube.com/watch?v=dmVd3pldqd0)

## Some Screenshots

### Collapsed Timeline

![Collapsed Timeline](media/collapsed.png)

### Expanded Item

![Expanded Item](media/expanded.png)

## File Overview

### LWC

* **timelineComponent** - Main component. Hosts a slds-timeline that iterates through retrieved activities and adds timelineItems for them.
* **timlineItem** - Component used by timelineComponent to generate each timeline item that displays.
* **recordLink** - Component for hyperlinking plain text to record pages utilizing NavigationMixin.

### Apex

* **TimelineController** - Controller for the component. Retrieves Tasks and Events for the component to display.
* **ActivityResponse** - Wrapper class used by the TimelineController to send a response back to the component.
* **RelatedData** - Wrapper class used by ActivityResponse containing data about related entities.